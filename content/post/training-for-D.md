---
title: Training for D
date: 2020-08-18
tags:
  - training
  - graduate
---

# Accounts
- Install a password manager, KeePass2 is a good one: https://keepass.info/download.html
- Create free GitLab account to store your code: https://gitlab.com/users/sign_up
- Generate a 100 random character password for your account using KeePass2

# Tasks
1. Update a [todo list](/post/todo)
1. Create a [new file](/post/newfile) with front matter: e.g., add a file `content/post/courses.md` and list of all the courses/subjects you have covered in the last two years
1. Merge requests TBC
